\select@language {english}
\contentsline {chapter}{List of Figures}{vi}{chapter*.2}
\contentsline {chapter}{List of Tables}{vii}{chapter*.3}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Motivation}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Contributions}{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Outline}{4}{subsection.1.1.3}
\contentsline {chapter}{\numberline {2}State of the Art}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Wireless Sensor Networks}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Arduino}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Arduino software}{9}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Arduino Sample source code}{9}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Arduino Shields}{10}{subsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.3.1}Network Shields}{11}{subsubsection.2.2.3.1}
\contentsline {subsubsection}{\numberline {2.2.3.2}Mega Sensor Shield}{12}{subsubsection.2.2.3.2}
\contentsline {subsection}{\numberline {2.2.4}Arduino Sensors}{12}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Arduino WiFly Wireless Module}{14}{subsection.2.2.5}
\contentsline {section}{\numberline {2.3}Time Series}{14}{section.2.3}
\contentsline {section}{\numberline {2.4}Time Series Analysis}{15}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Machine Learning}{15}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Artificial Neural Network}{16}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Fuzzy Logic}{16}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Regression Analysis}{18}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}Support Vector Machines}{18}{subsection.2.4.5}
\contentsline {section}{\numberline {2.5}Cloud Computing}{21}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Cloud services}{21}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Jabber}{22}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Summary}{23}{section.2.6}
\contentsline {chapter}{\numberline {3}Problem Statement}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}Research Question}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}Summary}{25}{section.3.2}
\contentsline {chapter}{\numberline {4}Contributions}{26}{chapter.4}
\contentsline {section}{\numberline {4.1}General Architecture}{26}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Aggregator's Architecture}{26}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}Training Algorithms}{30}{subsubsection.4.1.1.1}
\contentsline {subsection}{\numberline {4.1.2}Client's Architecture}{30}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Summary}{31}{section.4.2}
\contentsline {chapter}{\numberline {5}Case Studies}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}HTTP Communication}{33}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}HTTP communication without prediction logic}{33}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}HTTP communication with Linear regression and Fuzzy Logic}{33}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}XMPP Communication}{36}{section.5.2}
\contentsline {section}{\numberline {5.3}HTTP and Neural Network Communication}{37}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Training}{37}{subsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.1.1}Basic}{37}{subsubsection.5.3.1.1}
\contentsline {subsubsection}{\numberline {5.3.1.2}NEAT}{38}{subsubsection.5.3.1.2}
\contentsline {subsubsection}{\numberline {5.3.1.3}SVM backed Neural Network}{39}{subsubsection.5.3.1.3}
\contentsline {section}{\numberline {5.4}Validation}{40}{section.5.4}
\contentsline {section}{\numberline {5.5}Summary}{41}{section.5.5}
\contentsline {chapter}{\numberline {6}Related Work}{43}{chapter.6}
\contentsline {chapter}{\numberline {7}Conclusions and Future Research Directions}{45}{chapter.7}
\contentsline {chapter}{\numberline {8}Sisukokkuv\~{o}te}{47}{chapter.8}
\contentsline {chapter}{\numberline {9}License}{48}{chapter.9}
\contentsline {chapter}{Bibliography}{49}{chapter*.30}
